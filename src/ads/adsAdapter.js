/* global cast */

var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var AdsAdapter = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getPlayerName: function () {
    return 'Chromecast CAF Ads'
  },

  getPlayhead: function () {
    return this.player.getBreakClipCurrentTimeSec()
  },

  getDuration: function () {
    return this.player.getBreakClipDurationSec()
  },

  getPosition: function () {
    var adapter = this.plugin.getAdapter()
    var ret = youbora.Constants.AdPosition.Midroll
    if (adapter) {
      if (!adapter.flags.isJoined || adapter.getPlayhead() < 1) {
        ret = youbora.Constants.AdPosition.Preroll
      } else if (adapter.getPlayhead() + 1 >= adapter.getDuration()) {
        ret = youbora.Constants.AdPosition.Postroll
      }
    }
    return ret
  },

  getTitle: function () {
    return 'unknown'
  },

  getResource: function () {
    return 'unknown'
  },

  getCreativeId: function () {
    return this.clipId
  },

  getIsVisible: function () {
    return true
  },

  getIsFullscreen: function () {
    return true
  },

  getIsSkippable: function () {
    return this.isSkippable
  },

  getBreaksTime: function () {
    var breaks = this.player.getBreaks()
    var cuePoints = []
    for (var breakN in breaks) {
      var position = breaks[breakN].position
      if (position === -1 && this.plugin.getAdapter()) {
        position = this.plugin.getAdapter().getDuration()
      }
      if (cuePoints.indexOf(position) === -1) {
        cuePoints.push(position)
      }
    }
    return cuePoints
  },

  getGivenBreaks: function () {
    var breaks = this.player.getBreaks()
    var cuePoints = []
    for (var breakN in breaks) {
      var position = breaks[breakN].position
      if (position === -1 && this.plugin.getAdapter()) {
        position = this.plugin.getAdapter().getDuration()
      }
      if (cuePoints.indexOf(position) === -1) {
        cuePoints.push(position)
      }
    }
    return cuePoints.length
  },

  registerListeners: function () {
    var Events = cast.framework.events

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false, 1200)

    // Register listeners
    this.references = []
    this.references[Events.EventType.BREAK_ENDED] = this.endListener.bind(this)
    this.references[Events.EventType.BREAK_CLIP_LOADING] = this.playListener.bind(this)
    this.references[Events.EventType.BREAK_CLIP_STARTED] = this.joinListener.bind(this)
    this.references[Events.EventType.BREAK_CLIP_ENDED] = this.stopListener.bind(this)
    this.references[Events.EventType.PAUSE] = this.pauseListener.bind(this)
    this.references[Events.EventType.PLAYING] = this.playingListener.bind(this)

    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      this.references = []
    }
  },

  playListener: function (e) {
    this.plugin.getAdapter().fireStart()
    this.plugin.getAdapter().firePause()
    if (this.plugin.getAdapter().flags.isJoined) {
      this.fireBreakStart()
    }
    if (typeof e !== 'undefined') {
      this.isSkippable = e.whenSkippable !== undefined
      this.clipId = e.breakClipId
    } else {
      this.isSkippable = false
      this.clipId = null
    }
  },

  joinListener: function (e) {
    this._quartileTimer = new youbora.Timer(this.sendQuartile.bind(this), 1000)
    this._quartileTimer.start()
    this.plugin.getAdapter().firePause()
    this.fireStart()
    this.fireJoin()
  },

  stopListener: function (e) {
    this.fireStop()
    this._reset()
  },

  endListener: function (e) {
    this.fireBreakStop()
    this.plugin.getAdapter().fireResume()
  },

  sendQuartile: function (e) {
    var quartile = 0
    if (this.getPlayhead() > this.getDuration() / 4) {
      if (!this.firstQuartile) {
        this.firstQuartile = true
        quartile = 1
      }
      if (this.getPlayhead() > this.getDuration() / 2) {
        if (!this.secondQuartile) {
          this.secondQuartile = true
          quartile = 2
        }
        if (this.getPlayhead() > this.getDuration() * 0.75) {
          if (!this.thirdQuartile) {
            this.thirdQuartile = true
            quartile = 3
          }
        }
      }
    }
    this.fireQuartile(quartile)
    if (quartile === 3) this._quartileTimer.stop()
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    if (!this.flags.isBuffering) {
      this.firePause()
    }
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
  },

  _reset: function (e) {
    this.firstQuartile = false
    this.secondQuartile = false
    this.thirdQuartile = false
  }
})

module.exports = AdsAdapter
