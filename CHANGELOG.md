## [6.5.8] - 2020-02-18
### Fixed
- Removed all listeners for seek end, using only monitor
### Library
- Packaged with `lib 6.5.26`

## [6.5.7] - 2020-01-24
### Library
- Packaged with `lib 6.5.25`

## [6.5.6] - 2019-10-11
### Fixed
- GetHost check
### Library
- Packaged with `lib 6.5.16`

## [6.5.5] - 2019-09-05
### Added
- Catches pauses forced by autopause option when buffering
### Library
- Packaged with `lib 6.5.14`

## [6.5.4] - 2019-08-05
### Added
- Check for player showing wrong playrate when buffering or seeking
### Library
- Packaged with `lib 6.5.10`

## [6.5.3] - 2019-07-26
### Added
- Changed error messages
### Library
- Packaged with `lib 6.5.8`

## [6.5.2] - 2019-06-13
### Added
- Error messages following player documentation's ones
- Jointime detection using playhead

## [6.5.1] - 2019-06-11
### Fixed
- Check for play listener arguments not being null to prevent crashes

## [6.5.0] - 2019-06-07
### Added
- Native ad support
### Fixed
- Wrong throughput reported when chromecast returns fake value for decoded audio
### Library
- Packaged with `lib 6.5.3`

## [6.4.11] - 2019-05-22
### Library
- Packaged with `lib 6.4.29`

## [6.4.10] - 2019-05-10
### Fix
- Fixed cases where buffer is not detected because pause is reported instead
### Library
- Packaged with `lib 6.4.27`

## [6.4.8] - 2019-04-04
### Fix
- Seeked listener removed, using monitor instead to detect seeks
### Library
- Packaged with `lib 6.4.22`

## [6.4.7] - 2019-03-14
### Fix
- Plugin crashing when some errors happened
- Replaced CLIP_ENDED for MEDIA_FINISHED event

## [6.4.6] - 2019-03-13
### Fix
- Crash when trying to unregister before register listeners, with no reference to monitor.

## [6.4.5] - 2019-02-21
### Fix
- Added extra case for resource
### Library
- Packaged with `lib 6.4.17`

## [6.4.4] - 2019-01-25
### Fix
- Removed fake buffers after seek
### Library
- Packaged with `lib 6.4.13`

## [6.4.3] - 2018-12-20
### Fix
- Fixed bitrate number reported as rendition
### Library
- Packaged with `lib 6.4.12`

## [6.4.2] - 2018-10-01
### Fix
- Fixed error filter list

## [6.4.1] - 2018-09-25
### Added
- Error filter for non playback errors

## [6.4.0] - 2018-09-04
### Fix
- Error type parse fixed
### Library
- Packaged with `lib 6.4.6`


## [6.1.0] - 2017-12-21
### Library
- Packaged with `lib 6.1.7`
